/**
 * There are implementation of basic class Node
 * with two reference to next and to previous element
 * @param <T>
 */
public class Node<T> {
    private T value;
    private Node next;
    private Node prev;

    /**
     * Base constructor with NULL initialization
     */
    Node() {
        setValue(null);
        setNext(null);
        setPrev(null);
    }


    /**
     * Base constructor with initialization of "value"
     * @param value a value of Node
     */
    Node(T value) {
        setValue(value);
        setPrev(null);
        setNext(null);
    }


    Node(T value, Node<T> prev, Node<T> next) {
        setValue(value);
        setPrev(prev);
        setNext(next);
    }


    /**
     * Set value
     * @param value a value of Node
     */
    public void setValue(T value) { this.value = value; }


    /**
     * Set reference to next Node
     * @param next a reference to next Node
     */
    public void setNext(Node next) { this.next = next; }


    /**
     * Set reference to previous Node
     * @param prev a reference to previous Node
     */
    public void setPrev(Node prev) { this.prev = prev; }


    /**
     * Return value
     * @return return a value of Node (<T>)
     */
    public T getValue() { return value; }


    /**
     * Return reference to next Node
     * @return return a reference to next Node
     */
    public Node getNext() { return next; }


    /**
     * Return reference to previous Node
     * @return return a reference to previous Node
     */
    public Node getPrev() { return prev; }


    /**
     * Checks, has next Node or not
     * @return True of False
     */
    public boolean hasNext() { return getNext() != null; }


    /**
     * Checks, has previous Node or nor
     * @return True of False
     */
    public boolean hasPrev() { return getPrev() != null; }
}
